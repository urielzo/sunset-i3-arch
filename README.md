# sunset-minimalism

Archlinux i3 config

## Preview

## clean
![light](/preview/Clean.png)
<br />

## Dirty
![light](/preview/Dirty.png)
<br />

## Fonts for polybar

- **Shakerato-PERSONAL**
- **Material\-Design\-Iconic\-Font**
- **Font Awesome 5 Free**
- **feather**

## Fonts for conky
- **Bebas Neue**
- **Helvetica Neue**
- **font ShakeratoPERSONALBold**

## Details
- **Distro** ArchLinux ;)
- **WM** i3-gaps
- **Panel** Polybar
- **Weather Widgets and music player** Conky
- **Program Launcher** rofi


## You Need
- **Polybar**
- **Conky-lua-nv**
- **Mpd, Mpc, Ncmpcpp**
- **Rofi**
- **Reofetch**
- **i3-gaps**
- **Wezterm**
